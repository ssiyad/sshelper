package main

import (
	"fmt"
	"os"
	"strings"
)

func screenshot() (response string, err error) {
	date, _ := execute(`date '+%F-%T-%a'`)
	date = strings.TrimRight(date, "\n")
	home, _ := os.UserHomeDir()
	dest := fmt.Sprintf("%s/Screenshots/%s_full.png", home, date)

	_, err = execute(fmt.Sprintf("grim %s", dest))
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("Screenshot saved as %s", dest), nil
}
