package main

import (
	"log"
	"os/exec"
)

func execute(command string) (response string, err error) {
	// Have to find a way to perform wl-copy
	// Also refer https://github.com/bugaevc/wl-clipboard/issues/98
	stdout, err := exec.Command("sh", "-c", command).Output()

	if err != nil {
		log.Fatal(err)
	}

	return string(stdout), nil
}
