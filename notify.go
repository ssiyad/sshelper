package main

import "fmt"

func notify(heading string, message string) {
	execute(fmt.Sprintf("notify-send %q %q", heading, message))
}
