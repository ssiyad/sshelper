package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
)

type storage struct {
	driver   string
	dbType   string
	user     string
	password string
	host     string
	port     string
	dbName   string
	db       *sql.DB
}

func newDatabase(
	driver string,
	dbType string,
	user string,
	password string,
	host string,
	port string,
	dbName string,
) storage {
	db := storage{
		driver,
		dbType,
		user,
		password,
		host,
		port,
		dbName,
		nil,
	}
	res, err := sql.Open(
		db.driver,
		fmt.Sprintf(
			"%s://%s:%s@%s:%s/%s",
			db.dbType,
			db.user,
			db.password,
			db.host,
			db.port,
			db.dbName,
		),
	)
	if err != nil {
		log.Fatal(err)
	}
	db.db = res
	return db
}

func (database storage) status() map[string]int {
	db := database.db

	var (
		id   int
		name string
	)

	statusList := make(map[string]int)

	rows, err := db.Query("SELECT id, name FROM status")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&id, &name)
		if err != nil {
			log.Fatal(err)
		}
		statusList[name] = id
	}

	if rows.Err() != nil {
		log.Fatal(rows.Err())
	}

	return statusList
}

func (database storage) update(status int) {
	currentID, _ := database.current()
	if currentID != status {
		db := database.db

		time := time.Now().Format(time.RFC3339)

		_, err := db.Exec("INSERT INTO update (status, at) VALUES($1, $2)", status, time)

		if err != nil {
			log.Panicln(err)
		}
	}
}

func (database storage) current() (int, string) {
	db := database.db

	var (
		id     int
		status string
	)

	rows, err := db.Query("SELECT update.status, status.name FROM update, status WHERE status.id = update.status ORDER BY at DESC LIMIT 1")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&id, &status)
		if err != nil {
			log.Fatal(err)
		}
	}

	if rows.Err() != nil {
		log.Fatal(rows.Err())
	}

	return id, status
}
