package main

import (
	"fmt"
	"strings"
)

func windowScreenshot() (response string, err error) {
	size, _ := execute(`swaymsg -t get_tree | jq -r '.. | select(.focused?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"'`)
	size = strings.TrimRight(size, "\n")

	date, _ := execute(`date '+%F-%T-%a'`)
	date = strings.TrimRight(date, "\n")
	dest := fmt.Sprintf("$HOME/Screenshots/%s_window.png", date)

	_, err = execute(fmt.Sprintf("grim -g %q %q", size, dest))
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("Screenshot saved as %s", dest), nil
}
