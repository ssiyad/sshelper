package main

import (
	"os"
	"strings"
)

func main() {
	choice, _ := dmenu(choices())

	if len(choice) < 1 {
		os.Exit(1)
	}

	choice = strings.TrimRight(choice, "\n")

	res, _ := choiceList[strings.ToLower(choice)]()

	if len(res) > 0 {
		notify(choice, res)
	}
}

func config() map[string]string {
	return map[string]string{
		"SSHELPER_DB_DRIVER":   os.Getenv("SSHELPER_DB_DRIVER"),
		"SSHELPER_DB_TYPE":     os.Getenv("SSHELPER_DB_TYPE"),
		"SSHELPER_DB_USER":     os.Getenv("SSHELPER_DB_USER"),
		"SSHELPER_DB_PASSWORD": os.Getenv("SSHELPER_DB_PASSWORD"),
		"SSHELPER_DB_HOST":     os.Getenv("SSHELPER_DB_HOST"),
		"SSHELPER_DB_PORT":     os.Getenv("SSHELPER_DB_PORT"),
		"SSHELPER_DB_NAME":     os.Getenv("SSHELPER_DB_NAME"),
	}
}
