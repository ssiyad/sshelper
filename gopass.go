package main

import (
	"fmt"
	"strings"
)

func gopass() (response string, err error) {
	secrets, _ := execute("gopass list --flat")

	key, err := dmenu(secrets)
	if err != nil {
		return "", err
	}
	key = strings.TrimRight(key, "\n")

	info, err := execute(fmt.Sprintf("gopass show %s", key))
	if err != nil {
		return "", err
	}

	return info, nil
}
