# sshelper
## ssiyad's personal helper

### Requirements
- [go](https://golang.org/)
- [rofi](https://github.com/davatorium/rofi/)
- [grim](https://github.com/emersion/grim)
- [slurp](https://github.com/emersion/slurp)
- [sway](https://swaywm.org/)
- [jq](https://stedolan.github.io/jq/)