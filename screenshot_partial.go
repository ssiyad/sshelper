package main

import (
	"fmt"
	"strings"
)

func partialScreenshot() (response string, err error) {
	size, _ := execute("slurp")
	size = strings.TrimRight(size, "\n")

	date, _ := execute(`date '+%F-%T-%a'`)
	date = strings.TrimRight(date, "\n")
	dest := fmt.Sprintf("$HOME/Screenshots/%s_partial.png", date)

	_, err = execute(fmt.Sprintf("grim -g %q %q", size, dest))
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("Screenshot saved as %s", dest), nil
}
