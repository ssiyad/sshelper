package main

import (
	"strings"
)

var choiceList = map[string]func() (string, error){
	"full screenshot":    screenshot,
	"partial screenshot": partialScreenshot,
	"window screenshot":  windowScreenshot,
	"go-pass":            gopass,
	"activity tracker":   actracker,
}

func choices() string {
	res := ""
	for i := range choiceList {
		res += strings.Title(i) + "\n"
	}
	return strings.TrimRight(res, "\n")
}
