package main

import "fmt"

func dmenu(items string) (response string, err error) {
	return execute(fmt.Sprintf("echo -e %q | rofi -dmenu", items))
}
