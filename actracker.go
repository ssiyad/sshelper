package main

import (
	"fmt"
	"log"
	"strings"
)

var db = newDatabase(
	config()["SSHELPER_DB_DRIVER"],
	config()["SSHELPER_DB_TYPE"],
	config()["SSHELPER_DB_USER"],
	config()["SSHELPER_DB_PASSWORD"],
	config()["SSHELPER_DB_HOST"],
	config()["SSHELPER_DB_PORT"],
	config()["SSHELPER_DB_NAME"],
)

func actracker() (response string, err error) {
	status := db.status()
	menu := ""
	for name := range status {
		menu += name + "\n"
	}
	out, err := dmenu(strings.TrimRight(menu, "\n"))
	if err != nil {
		log.Fatal(err)
	}
	out = strings.Trim(out, "\n")

	db.update(status[out])

	return fmt.Sprintf("Status set to %s", out), nil
}
